#!/bin/bash

sudo yum install python36 -y
sudo dnf install python3-pip -y
sudo yum install tmux -y
pip3 install torch==1.9.0+cpu torchvision==0.10.0+cpu torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html --no-cache-dir --user
pip3 install matplotlib --no-cache-dir --user
