import torch
import pickle
import random

'''Reads in muon files into a dictionary of tensors and saves them as a pickled object for later use'''
class muon_reader:
    def __init__(self): 
        #self.muon_data = {'positive':None,'negative':None}
        self.muon_data = {'training':{'positive':None,'negative':None},'unbiased':{'positive':None,'negative':None}}

    def read_and_save(self,positive_filename,negative_filename,output_filename,training_fraction):
        if not isinstance(training_fraction,float) or (training_fraction > 1.0 and training_fraction < 0):
            raise RuntimeError('muon_reader: training_fraction must be between 0 and 1')

        self.read_data(positive_filename,negative_filename,training_fraction)
        self.save(output_filename)

    def read_data(self,positive_filename,negative_filename,training_fraction):
        self.muon_data['training']['positive'],self.muon_data['unbiased']['positive'] = self._read_into_tensor(positive_filename,training_fraction)
        self.muon_data['training']['negative'],self.muon_data['unbiased']['negative'] = self._read_into_tensor(negative_filename,training_fraction)
        
    def save(self,output_filename):
        with open(output_filename,'wb') as out_file:
            pickle.dump(self.muon_data,out_file,pickle.HIGHEST_PROTOCOL)


    def _read_into_tensor(self,filename,training_fraction):
        '''
        Assumes the following structure:
        col 1 : run number
        col 2 : event number
        col 3 : particle types (10=muon+,11=muon-)
        col 4 : generation number
        col 5 : kinetic energy in GeV
        col 6 : statistical weight
        col 7 : x coordinate
        col 8 : y coordinate
        col 9 : direction cosine wrt x-axis
        col 10: direction cosine wrt y-axis
        col 11: particle age in seconds
        col 12: ignore

        The probability an event will be included is current_weight/largest_weight
        '''
        all_data = torch.empty(0,6)
        training_data = torch.empty(0,5)
        unbiased_data = torch.empty(0,5)
        largest_weight = -1
        with open(filename,'r') as file:
            for line in file:
                line = line.strip()
                line = [''] + [float(col) for col in line.split(' ') if col != '']
                
                #new_row = torch.tensor([[line[5],line[6],line[7],line[8],line[9],line[10]]])
                new_row = torch.tensor([line[5:11]])
                
                all_data = torch.cat((all_data,new_row))
                
                largest_weight = line[6] if line[6] > largest_weight else largest_weight
        
        for event in all_data:
            current_weight = event[1]
            prob = current_weight/largest_weight
            roll = random.random()
            
            event = torch.cat((event[:1],event[2:])) #remove statistical weight
            event = torch.reshape(event,(1,5))
            if prob >= roll:
                dest = random.random()
                if dest <= training_fraction:
                    training_data = torch.cat((training_data,event))
                else:
                    unbiased_data = torch.cat((unbiased_data,event))

        return (training_data,unbiased_data)

if __name__ == "__main__":
    reader = muon_reader()
    reader.read_and_save('unit30_Pm','unit30_Nm','muon_data_2.pkl',.8)
