#!/bin/bash

source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python
sh /afs/cern.ch/user/a/abarkley/private/gan-testing/Miniconda3-latest-Linux-x86_64.sh

echo $1

python /afs/cern.ch/user/a/abarkley/private/gan-testing/muon/generate_sample.py -g -i 1 -e 5000 -f /afs/cern.ch/user/a/abarkley/private/gan-testing/muon/muon_data_2.pkl --graph_path /afs/cern.ch/user/a/abarkley/private/gan-testing/muon/graphs --result_path /afs/cern.ch/user/a/abarkley/private/gan-testing/muon/results -n $1
