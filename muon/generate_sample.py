import torch
from torch import nn

import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

import argparse

from gan_muon import Gan_Trainer
from gan_muon import Gan_Statistics

from muon_distribution import muon_data

import numpy as np

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(37,256), 
            #nn.Linear(62,256),
            #nn.Linear(17,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,5)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-N','-n','--name',default='gan',help='name of the results file and graph')
    parser.add_argument('-f',help='muon distribution file (should be a .pkl file)',required=True)
    parser.add_argument('-pos',action='store_true',help='set to make sample of positive muons')
    parser.add_argument('-neg',action='store_true',help='set to make sample of negative muons')
    parser.add_argument('-i',default = 1,type=int,help='number of gans to train')
    parser.add_argument('-e,--epochs',default = 1000,type=int,help='number of epochs to train for')
    parser.add_argument('--graph_path',default="/home/barkleya/gan/muon/graphs",help='location where graph will be saved')
    parser.add_argument('--result_path',default="/home/barkleya/gan/muon/results",help='location where the results file will be saved')
    parser.add_argument('-d',action='store_true',help='set to compare cdfs')
    parser.add_argument('-g',action='store_false',help='set to not draw graphs')
    parser.add_argument('-s',action='store_true',help='set to use a seed')
    parser.add_argument('-v',action='store_true',help='set to display more information during the run')
    args = vars(parser.parse_args())
    
    
    #training_sample = distribution.camel_back_2d(1024)
    #verification_sample = distribution.camel_back_2d(512)
    
    muon_dist_filename = args['f']
    make_pos_sample = args['pos']
    #make_neg_sample = args['neg']
    if make_pos_sample:
        dist = muon_data(muon_dist_filename,'p',['energy','x','y','cosign_x','cosign_y'])
        if make_neg_sample:
            print("both -pos and -neg flags set, only making positive sample")
    else:
        dist = muon_data(muon_dist_filename,'n',['energy','x','y','cosign_x','cosign_y'])
    
    compare_cdfs = args['d']
    gstats = Gan_Statistics()
    sample_cdf = []
    x = []
    y = []
    if compare_cdfs:
        x = np.linspace(-1,1)
        y = np.linspace(-1,1)
        x,y = np.meshgrid(x,y,indexing='ij')
        x = x.ravel()
        y = y.ravel()

        sample_cdf = gstats.cdf(verification_sample,x,y)
    
    batch_size = 350
    data_size = dist.training_data.size(dim=0) 
    if data_size % batch_size != 0:
       dist.resize_training_data(data_size - (data_size % batch_size)) 

    iterations = args['i']
    for i in range(0,iterations):
        trainer = Gan_Trainer(seeded=args['s'])
        trainer.train(Generator(),
                      Discriminator(),
                      #distribution=distribution.camel_back_2d,
                      training_data = dist.training_data,
                      data_shift=[lambda x : x*1, lambda x : y*1, lambda x : x*1, lambda x : x*1, lambda x : x*1],
                      num_epochs=args['e,__epochs'],
                      #batch_size=512,
                      #data_size=1024,
                      batch_size=350,
                      data_size=3500,
                      lr = [5*10**-3,5*10**-5],
                      iteration=i,
                      xlim=[-10,10],
                      ylim=[-10,10],
                      graph_path = args['graph_path'],
                      results_path = args['result_path'],
                      name = args['name'],
                      compare_cdfs = compare_cdfs,
                      gstats = gstats,
                      validation_cdf = sample_cdf,
                      cdf_xs = x,
                      cdf_ys = y,
                      draw_graphs = args['g'],
                      verbose = args['v'])

        
        #generated_sample = trainer.sample_distribution(verification_sample)

        #generated_sample = trainer.generate_samples(512)
        #generated_cdf = gstats.cdf(generated_sample,x,y)
        
        #print("max difference between the CDFs are " + str(max(abs(generated_cdf - sample_cdf))))
        #print("graphing CDF")

        #fig = plt.figure(figsize=(15,10))
        #ax_sample = fig.add_subplot(2,1,1,projection='3d')
        #ax_generated = fig.add_subplot(2,1,2,projection='3d')

        #ax_sample.plot_trisurf(x,y,sample_cdf)
        #ax_generated.plot_trisurf(x,y,sample_cdf)
        #plt.show()
