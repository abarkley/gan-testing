import torch
import pickle

class muon_data:
    def __init__(self,data_filename,muon_type = None,dimensions=None):
        if muon_type == None or dimensions == None:
            raise AttributeError('muon_distribution: muon_type (either \'p\'or \'n\') and dimensions (list with a combination of the following: energy, x, y, cosign_x, cosign_y) must be specified')
        
        self._load_data(data_filename)
        self._training_data = self.muon_data['training']
        self._unbiased_data = self.muon_data['unbiased']

        self._cut(muon_type,dimensions)
        self._data_shifts = self._normalize_training_data()

    @property
    def training_data(self):
        return self._training_data
    @property
    def unbiased_data(self):
        return self._unbiased_data
    @property
    def data_shifts(self):
        return self._data_shifts
   
    def resize_training_data(self,new_size):
        current_size = self._training_data.size(dim=0)
        if new_size > current_size:
            raise ValueError("muon distribution must be resized to a smaller value")

        self._training_data.resize_(new_size,5)

    def _load_data(self,data_filename):
        with open(data_filename,'rb') as in_file:
            self.muon_data = pickle.load(in_file)
    
    def _cut(self,muon_type,dimensions):
        self._cut_type(muon_type) 
        self._cut_dimensions(dimensions)

    def _cut_type(self,muon_type):
        positive = muon_type == 'p'
        negative = muon_type == 'n'
        
        if not positive and not negative:
            raise AttributeError('muon_distribution: muon_type (either \'p\' or \'n\')')
        
        if positive:
            self._training_data = self._training_data['positive']
            self._unbiased_data = self._unbiased_data['positive']
        elif negative:
            self._training_data = self._training_data['negative']
            self._unbiased_data = self._unbiased_data['negative']
    
    def _cut_dimensions(self,dimensions):
        training = []
        unbiased = []
        training_rows = self._training_data.size()[0]
        unbiased_rows = self._unbiased_data.size()[0]
        for dimension in dimensions:
            if dimension is 'energy':
                training.append(self._training_data[:,0].reshape(training_rows,1))
                unbiased.append(self._unbiased_data[:,0].reshape(unbiased_rows,1))
            elif dimension is 'x':
                training.append(self._training_data[:,1].reshape(training_rows,1))
                unbiased.append(self._unbiased_data[:,1].reshape(unbiased_rows,1))
            elif dimension is 'y':
                training.append(self._training_data[:,2].reshape(training_rows,1))
                unbiased.append(self._unbiased_data[:,2].reshape(unbiased_rows,1))
            elif dimension is 'cosign_x':
                training.append(self._training_data[:,3].reshape(training_rows,1))
                unbiased.append(self._unbiased_data[:,3].reshape(unbiased_rows,1))
            elif dimension is 'cosign_y':
                training.append(self._training_data[:,4].reshape(training_rows,1))
                unbiased.append(self._unbiased_data[:,4].reshape(unbiased_rows,1))
            else:
                raise AttributeError(f'muon_distribution: unknown dimension {dimension}')

        self._training_data = torch.cat(training,1)
        self._unbiased_data = torch.cat(unbiased,1)

    def _normalize_training_data(self):
        data_shifts = []
        for dimension in range(0,self._training_data.size()[1]): 
            data_shifts.append(torch.max(torch.abs(self._training_data[:,dimension])).item())
        self._training_data = torch.div(self._training_data,torch.tensor(data_shifts))
        return data_shifts 

if __name__ == "__main__":
    dist = muon_data('muon_data_2.pkl','p',['energy','x','y','cosign_x','cosign_y']) 
    print(dist.data_shifts)
    print(torch.max(dist.training_data))
    print(torch.min(dist.training_data))
    print(dist.training_data)
    print(dist.unbiased_data)
    print(dist.training_data.size())

    dist.resize_training_data(7000)
    print(dist.training_data)
    print(dist.unbiased_data)

    print(dist.training_data.size())

