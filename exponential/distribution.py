import torch
from torch import nn
from torch.distributions.exponential import Exponential

def exponential(size):
    return (Exponential(1).sample((size,)) )/ 6 -.5