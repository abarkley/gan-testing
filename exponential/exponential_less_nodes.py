import torch
from torch import nn

import matplotlib.pyplot as plt

import distribution

from gan_1D import Gan_Trainer

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(33,256), 
            #nn.Linear(1,256), 
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
           # nn.Linear(256,256),
           # nn.LeakyReLU(),
           # nn.Dropout(0.50),
            nn.Linear(256,1)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    trainer = Gan_Trainer(seeded=True)
    trainer.train(Generator(),
                  Discriminator(),
                  distribution=distribution.exponential,
                  #data_shift= lambda x : (x+1)*3,
                  data_shift= lambda x : (x+.5)*6,
                  batch_size=10,
                  data_size=150,
                  lr = [5*10**-3,5*10**-6 * .9**10],
                  xlim=[-1,4],
                  ylim=[0,50],
                  quantiles=[0,.105361,.223144,.356675,.510826,.693147,.916291,1.20397,1.60944,2.30259])