import torch
from torch import nn

import matplotlib.pyplot as plt

import distribution

from gan_entire_batch import Gan_Trainer

class Discriminator(nn.Module):
    def __init__(self,batch_size):
        super().__init__()
        self.model = nn.Sequential(
            #nn.Linear(33,256), 
            #nn.Linear(1,256), 
            nn.Linear(batch_size,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self,batch_size):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(256,batch_size),
            nn.LeakyReLU(),
            nn.Dropout(0.25),
            nn.Linear(batch_size,1)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    trainer = Gan_Trainer(seeded=True)
    batch_size = 100
    trainer.train(Generator(batch_size),
                  Discriminator(batch_size),
                  distribution=distribution.exponential,
                  data_shift= lambda x : (x+.5)*6,
                  batch_size=batch_size,
                  data_size=1000,
                  num_epochs=10000,
                  lr = [5*10**-4,5*10**-5 * .9**10],
                  xlim=[-1,4],
                  ylim=[0,300],
                  quantiles=[0,.105361,.223144,.356675,.510826,.693147,.916291,1.20397,1.60944,2.30259])