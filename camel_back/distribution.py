import torch
from torch import nn

def camel_back(size):
    sample1 = torch.normal(4.0,.5,(size//2,)) /8
    sample2 = torch.normal(-4.0,.5,(size//2,)) /8
    dist = torch.cat((sample1,sample2),0)
    return dist

def wide_left_camel_back(size):
    sample1 = torch.normal(4.0,.25,(size//2,)) /8
    sample2 = torch.normal(-4.0,.75,(size//2,)) /8
    dist = torch.cat((sample1,sample2),0)
    return dist

def wide_right_camel_back(size):
    sample1 = torch.normal(4.0,.75,(size//2,)) /8
    sample2 = torch.normal(-4.0,.25,(size//2,)) /8
    dist = torch.cat((sample1,sample2),0)
    return dist

def shift_right_camel_back(size):
    sample1 = torch.normal(8.0,.50,(size//2,)) /12
    sample2 = torch.normal(-4.0,.50,(size//2,)) /12
    dist = torch.cat((sample1,sample2),0)
    return dist