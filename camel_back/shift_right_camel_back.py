import torch
from torch import nn

import matplotlib.pyplot as plt

import distribution

from gan_1D import Gan_Trainer

class Discriminator(nn.Module):
    def __init__(self,batch_size):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(33,256), 
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.ELU(),
            nn.Dropout(0.5),
            nn.Linear(256,1)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    batch_size = 10
    trainer = Gan_Trainer(seeded=True)
    trainer.train(Generator(),
                  Discriminator(batch_size),
                  distribution=distribution.shift_right_camel_back,
                  data_shift=lambda x : x*12,
                  batch_size=batch_size,
                  data_size=100,
                  lr = [5*10**-5,5*10**-5 * .9**10],
                  xlim=[-12,12],
                  ylim=[0,.7],
                  quantiles=[-10,-4.59512,-4.17914,-3.82086,-3.40488,1.90109,7.40488,7.82086,10])