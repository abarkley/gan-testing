from distribution import camel_back_2d
import matplotlib.pyplot as plt
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-N','-n','--name',default='gan')
    parser.add_argument('-g',action='store_false')
    args = vars(parser.parse_args())
    print(args)
    print(args['name'])

    dist = camel_back_2d(1000)
    fig,axs = plt.subplots(2,3)
    axs[1,1] = plt.hist2d(list(dist[:,0]),list(dist[:,1]),bins=[100,100])
    path = "/home/barkleya/gan/graphs/gauss_2d/"
    name = "test_fig"
    if path[-1] is not '/':
        path += '/'
    print(path+name)
    #plt.show()   
    fig.savefig(path + name + ".png")

