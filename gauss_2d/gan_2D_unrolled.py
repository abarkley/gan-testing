import torch
from torch import nn
from torch.optim.lr_scheduler import LambdaLR,ExponentialLR

import math
import matplotlib.pyplot as plt

import os
from collections import deque
import copy

from distribution import camel_back_2d

class Gan_Trainer:
    class Grapher:
        def __init__(self,dir_path,file_name):
            self.dir_path = dir_path
            self.file_name = file_name
            
            if not os.path.isdir(self.dir_path):
                raise ValueError(f"directory {self.dir_path} does not exist") 
            if os.path.isfile(self.file_name):
                raise ValueError(f"graph {self.dir_path+self.file_name} exists") 
            
            if self.dir_path[-1] is not '/':
                self.dir_path+='/'

            plt.ion()
            self.fig,self.axs = plt.subplots(2,3)
            self.fig.set_size_inches(16,20)

            self.ax_distribution = self.axs[0,0]
            self.ax_chi = self.axs[0,1]
            self.ax_mean = self.axs[0,2]
            self.ax_violations = self.axs[1,0]
            self.ax_hist = self.axs[1,1]
            self.ax_mse = self.axs[1,2]

            self.ax_distribution.set_title("distribution")
            self.ax_chi.set_title("Chi Error")
            self.ax_mean.set_title("Chi Error Mean")
            self.ax_violations.set_title("Elements outside of range")
            self.ax_hist.set_title("Distrbution of generated points")
            self.ax_mse.set_title("Mean squared error")

        def plot_distribution(self,samples,shift_functions,xlim,ylim):
            #self.ax_distribution.set_ylim(ylim)
            #self.ax_distribution.set_xlim(xlim)
            self.ax_distribution.set_autoscale_on(False)
            
            self.hist(self.ax_distribution,samples,shift_functions,[xlim,ylim])
            #x = list(shift_functions[0](samples[:,0]))
            #y = list(shift_functions[1](samples[:,1]))

            #self.ax_distribution.hist2d(x,y,bins = [100,100])
            
        def plot_chi_points(self,chi,epoch):
            self.ax_chi.plot(epoch,chi,'.')

        def plot_mean_chi_points(self,chi_avg,epoch):
            self.ax_mean.plot(epoch,chi_avg,'.')
        
        def plot_elements_out_of_range(self,num_violations,epoch):
            self.ax_violations.plot(epoch,num_violations,'.')

        def plot_generated_points(self,generate_samples,shift_functions,xlim,ylim):
            self.ax_hist.clear()
            self.ax_hist.set_title("Distrbution of generated points")
            #self.ax_hist.set_ylim(ylim)            
            #self.ax_hist.set_xlim(xlim)

            self.hist(self.ax_hist,generate_samples,shift_functions,[xlim,ylim])
            #self.ax_hist.hist(shift_function(generate_samples[:,0]),20,xlim)

        def plot_mse(self,mse,epoch):
            self.ax_mse.plot(epoch,mse,'.')

        def redraw(self):
            self.fig.canvas.draw()

        def hist(self,axs,samples,shift_functions,range):
            x = list(shift_functions[0](samples[:,0]))
            y = list(shift_functions[1](samples[:,1]))

            axs.hist2d(x,y,bins = [100,100],range = range)

        def save_plot(self):
            self.fig.savefig(self.dir_path+self.file_name+".png")

    class Data_Analyser:
        def __init__(self,num_epochs):
            self.num_data_points = num_epochs / 100
            self.chi_data = deque()
            self.chi_avg = 0

            self._chi_squared = None
            self._sma = None
            self._outside_valid_range = None
            self._mse = None
            self._sample = None
            self._positive_negative = {'sample': {'x': {'positive':None,'negative':None}, 'y': {'positive':None,'negative':None}},'gan': {'x': {'positive':None,'negative':None}, 'y': {'positive':None,'negative':None}}}

        def chi_squared(self,generated_samples, func, shift_function):
            # Assuming right now that the generated_samples are 2 dimensional.
            n = generated_samples[:,0].size()[0]
            chi_squared = 0
            for i in range(0,n):
                x = generated_samples[:,0][i].item()
                gen_y = generated_samples[:,1][i].item()
                y = func(shift_function(torch.tensor(x)))
                chi_squared += ((gen_y-y)**2)/abs(y)
            self._chi_squared = chi_squared
            return chi_squared

        def sma(self,chi):
            if len(self.chi_data) == self.num_data_points:
                last = self.chi_data.pop()
                self.chi_avg += (1/self.num_data_points) * (chi-last)
            else:
                self.chi_data.insert(0,chi)
                self.chi_avg = sum(self.chi_data) / len(self.chi_data)
            self._sma = self.chi_avg
            return self.chi_avg

        def outside_valid_range(self,generated_samples):
            valid_range = [-1,1]
            x = generated_samples[:,0].tolist()
            self._outside_valid_range= len(list(filter(lambda elem: elem < valid_range[0] or elem > valid_range[1],x)))
            return self._outside_valid_range

        def mse(self,samples,quantiles,data_shifts,is_sample=False):
            error = 0
            for i in range(0,len(quantiles)):
                sample = samples[:,i]
                sample = data_shifts[i](sample)
                #get probabilities
                p_sample = []
                quantile = quantiles[i]
                p_sample.append(len(list(filter(lambda x: x<quantile[0],sample)))/len(sample))
                for q in range(0,len(quantile)-1):
                    p_sample.append(len(list(filter(lambda x: quantile[q]<=x<=quantile[q+1],sample)))/len(sample))
                p_sample.append(len(list(filter(lambda x: x>quantile[-1],sample)))/len(sample))
                # error for each probability
                #p_sample[0] = p_sample[0]**2
                prob = 1 / (100/(len(quantile)+1))
                for p in range(0,len(p_sample)):
                    p_sample[p] = (p_sample[p] - prob)**2
                error += 1/10 * sum(p_sample)
            if is_sample:
                self._sample = error
            else:
                self._mse = error
            
            return error

        def num_negative_positive(self,samples,is_x,is_sample=False):
            positive_mask = samples.ge(0) 
            positive = torch.masked_select(samples,positive_mask)

            negative_mask = samples.le(0)
            negative = torch.masked_select(samples,negative_mask)
            if is_sample:
                if is_x:
                    self._positive_negative['sample']['x']['positive'] = positive.size()[0]
                    self._positive_negative['sample']['x']['negative'] = negative.size()[0]
                else:
                    self._positive_negative['sample']['y']['positive'] = positive.size()[0]
                    self._positive_negative['sample']['y']['negative'] = negative.size()[0]
            else:
                if is_x:
                    self._positive_negative['gan']['x']['positive'] = positive.size()[0]
                    self._positive_negative['gan']['x']['negative'] = negative.size()[0]
                else:
                    self._positive_negative['gan']['y']['positive'] = positive.size()[0]
                    self._positive_negative['gan']['y']['negative'] = negative.size()[0]
            return (positive.size()[0],negative.size()[0])


        def get_results(self,categories,iteration):
            results = {'iteration':iteration} 
            for category in categories:
                if category is "chi_squared":
                    results['chi_squared']  = self._chi_squared
                elif category is "sample_mse":
                    results['sample_mse'] = self._sample
                elif category is "sma":
                    results['sma'] = self._sma
                elif category is "outside_valid_range":
                    results["outside_valid_range"] = self._outside_valid_range
                elif category is "mse":
                    results['mse'] = self._mse

                elif category is "sample_positive_x":
                    results["sample_positive_x"] = self._positive_negative['sample']['x']['positive']
                elif category is "sample_negative_x":
                    results["sample_negative_x"] = self._positive_negative['sample']['x']['negative']
                elif category is "sample_positive_y":
                    results["sample_positive_y"] = self._positive_negative['sample']['y']['positive']
                elif category is "sample_negative_y":
                    results["sample_negative_y"] = self._positive_negative['sample']['y']['negative']

                elif category is "positive_x":
                    results["positive_x"] = self._positive_negative['gan']['x']['positive']
                elif category is "negative_x":
                    results["negative_x"] = self._positive_negative['gan']['x']['negative']
                elif category is "positive_y":
                    results["positive_y"] = self._positive_negative['gan']['y']['positive']
                elif category is "negative_y":
                    results["negative_y"] = self._positive_negative['gan']['y']['negative']

            return results
    
    class Results:
        def __init__(self,categories,results,file_path):
            self.categories = categories
            self.results = results
            self.file_path = file_path

        #def __str__(self):  
        #    print(self._results[0])
        #    for result in self._results[1:]:
        #        print(f",{result}")

        def csv_print(self):
            file = None
            try:
                if not os.path.isfile(self.file_path):
                    file = open(self.file_path,'w')                
                    file.write(self.categories[0])
                    for category in self.categories[1:]:
                        file.write(f",{category}")
                    file.write('\n')
                else:
                    file = open(self.file_path,'a')
                    file.write('\n')

                file.write(f"{self.results['iteration']}")
                for category in self.categories[1:]:
                    file.write(f",{self.results[category]}")
                file.close()

            except:
                file.close()
                raise

    def __init__(self,seeded=False):
        self.default_quantile = [-4.84162,-4.25335,-3.74665,-3.15838,0,3.15838,3.74665,4.25335,4.84162]
        if seeded:
            torch.manual_seed(111)

    def smooth_learning_rate(self,epoch):
        return math.exp(self.m * (epoch - 1))

    def data_setup(self,distribution,data_shift,batch_size,data_size):
        train_data_length = data_size
        self.train_data = torch.zeros((train_data_length,2))

        self.train_data = distribution(train_data_length)
        #self.train_data[:, 0] = (torch.rand(train_data_length)-.5)*2 
        #self.train_data[:, 1] = data_function(data_shift(self.train_data[:,0]))

        train_labels = torch.zeros(train_data_length)
        train_set = [
            (self.train_data[i],train_labels[i]) for i in range(train_data_length)
        ]
        self.train_loader = torch.utils.data.DataLoader(train_set,batch_size=batch_size,shuffle=True)

        self.real_samples_labels = torch.ones((batch_size, 1))

    def train(self,
              generator,
              discriminator,
              distribution = camel_back_2d,
              data_shift = lambda x : x*6,
              num_epochs=1000,
              batch_size=32,
              data_size=1024,
              lr=[0.001,0.001],
              loss_function = nn.BCELoss,
              iteration = 0,
              roll_steps=5,
              result_categories = ["iteration",
                                   "sample_mse",
                                   "sample_positive_x",
                                   "sample_negative_x",
                                   "sample_positive_y",
                                   "sample_negative_y",
                                   "mse",
                                   "positive_x",
                                   "negative_x",
                                   "positive_y",
                                   "negative_y"],

              xlim = [-1,4],
              ylim = [0,50],
              quantiles = None,
              graph_path = "/home/barkleya/gan/graphs/gauss_2d/",
              results_path = "/home/barkleya/gan/gauss_2d/results",
              name = "gan",
              draw_graphs = True,
              verbose = False):
        
        print(f"------iteration {iteration}------")

        if quantiles == None:   
            quantiles = [self.default_quantile,self.default_quantile]

        self.graphing_batch_size = 1000

        self.grapher = self.Grapher(graph_path,f"{name}_{iteration}")
        self.data_analyser = self.Data_Analyser(num_epochs)
        
        self.loss_function = loss_function
        self.loss_function_generator = loss_function()
        self.loss_function_discriminator = loss_function()
        self.generator = generator
        self.discriminator = discriminator

        self.data_setup(distribution,data_shift,batch_size,data_size)

        self.sample_mse = self.data_analyser.mse(self.train_data,quantiles,data_shift,is_sample=True)
        if verbose:
            print(f"mse: {self.sample_mse}")
        self.data_analyser.num_negative_positive(self.train_data[:,0],is_x=True,is_sample=True)
        self.data_analyser.num_negative_positive(self.train_data[:,1],is_x=False,is_sample=True)
        
        self.grapher.plot_distribution(self.train_data, data_shift, xlim, ylim)

        if type(lr) is list:
            self.lr_init = lr[0]
            self.lr_end = lr[1]
        else:
            self.lr_init = self.lr_end = lr
        self.m = math.log(self.lr_end/self.lr_init) / (num_epochs-1)

        #TODO: be able to change this?
        self.optimizer_discriminator = torch.optim.Adam(discriminator.parameters(),lr=self.lr_init)
        self.optimizer_generator = torch.optim.Adam(generator.parameters(),lr=self.lr_init)

        self.scheduler_discriminator = LambdaLR(self.optimizer_discriminator, self.smooth_learning_rate)
        self.scheduler_generator = LambdaLR(self.optimizer_generator, self.smooth_learning_rate)

        for epoch in range(num_epochs):
            if verbose:
                print("new epoch")

            for n, (real_samples, _) in enumerate(self.train_loader):
                loss_discriminator = self.train_discriminator(batch_size,real_samples)
                loss_generator,_= self.train_generator(batch_size,real_samples,roll_steps)
                
                # Show loss
                if epoch % 10 == 0 and n == (data_size//batch_size) - 1:
                                       
                    generate_samples = self.generate_samples(batch_size)

                    #chi = self.data_analyser.chi_squared(generate_samples,data_function,data_shift)
                    #chi_avg = self.data_analyser.sma(chi)
                    #num_violations = self.data_analyser.outside_valid_range(generate_samples)
                    mse = self.data_analyser.mse(generate_samples,quantiles,data_shift)
                    self.data_analyser.num_negative_positive(generate_samples[:,0],is_x=True)
                    self.data_analyser.num_negative_positive(generate_samples[:,1],is_x=False)

                    if verbose:
                        
                        print(f"Epoch: {epoch} Loss D.: {loss_discriminator}")
                        print(f"Epoch: {epoch} Loss G.: {loss_generator}")
                        print(f"Epoch: {epoch} lr: {self.optimizer_generator.param_groups[0]['lr']}")
                        print(f"Epoch: {epoch} mse: {mse}")
                        #print(f"Epoch: {epoch} violations: {num_violations}")
                        #print(f"Epoch: {epoch} chi squared error.: {chi}")
                        #print(f"Epoch: {epoch} chi squared error mean.: {chi_avg}")

                    self.grapher.plot_mse(mse, epoch)
                    if draw_graphs:
                        #self.grapher.plot_generated_points(generate_samples,data_shift,xlim,ylim)
                        #self.grapher.plot_chi_points(chi,epoch)
                        #self.grapher.plot_mean_chi_points(chi_avg,epoch)
                        #self.grapher.plot_elements_out_of_range(num_violations,epoch)
                        #self.grapher.plot_distribution(real_samples, data_shift, xlim, ylim)

                        self.grapher.plot_generated_points(generate_samples,data_shift,xlim,ylim)
                        self.grapher.redraw()

            self.scheduler_discriminator.step()
            self.scheduler_generator.step()

       
        self.grapher.plot_generated_points(generate_samples,data_shift,xlim,ylim)
        self.grapher.plot_mse(mse, epoch)
        self.grapher.redraw()
        self.grapher.save_plot()

        if draw_graphs:
            plt.show(block=True)

        if results_path[-1] is not '/':
            results_path += '/'

        data_results = self.data_analyser.get_results(result_categories,iteration)
        results = self.Results(result_categories,data_results,results_path+name+'.results')
        results.csv_print()

        #snapshot = tracemalloc.take_snapshot()
        #top = snapshot.statistics('lineno')
        #for stat in top[:10]:
        #    print(stat)
        


    def train_discriminator(self,batch_size,real_samples):
        # Data for training the discriminator
        latent_space_samples = torch.randn((batch_size, 1))
        generated_samples = self.generator(latent_space_samples)
        generated_samples_labels = torch.zeros((batch_size, 1))

        generated_samples = self.per_batch_statistics(generated_samples)
        real_samples = self.per_batch_statistics(real_samples)

        all_samples = torch.cat((real_samples, generated_samples))
        all_samples_labels = torch.cat(
            (self.real_samples_labels, generated_samples_labels)
        )
        
        # Training the discriminator
        self.discriminator.zero_grad()
        output_discriminator = self.discriminator(all_samples)
        loss_discriminator = self.loss_function_discriminator(
            output_discriminator, all_samples_labels)
        loss_discriminator.backward()
        self.optimizer_discriminator.step()
        return loss_discriminator
    
    def train_discriminator_unrolled(self,discriminator_backup,optimizer_discriminator_unrolled,scheduler_discriminator_unrolled,batch_size,real_samples,generated_samples,loss_discriminator_unrolled,iter,roll_steps):
        # Data for training the discriminator
        latent_space_samples = torch.randn((batch_size, 1))
        #generated_samples = self.generator(latent_space_samples)
        generated_samples_labels = torch.zeros((batch_size, 1))

        #generated_samples = self.per_batch_statistics(generated_samples)
        real_samples = self.per_batch_statistics(real_samples)

        all_samples = torch.cat((real_samples, generated_samples))
        all_samples_labels = torch.cat(
            (self.real_samples_labels, generated_samples_labels)
        )
        
        # Training the discriminator
        #self.discriminator.zero_grad()
        #output_discriminator = self.discriminator(all_samples)
        discriminator_backup.zero_grad()
        output_discriminator = discriminator_backup(all_samples)

        #loss_discriminator = self.loss_function_discriminator(
        loss_discriminator = loss_discriminator_unrolled(
            output_discriminator, all_samples_labels)
        loss_discriminator.backward(retain_graph=True)

        optimizer_discriminator_unrolled.step()
        optimizer_discriminator_unrolled.zero_grad()

        return loss_discriminator

    def per_batch_statistics(self,samples):
        difference = self.calculate_difference(samples)
        embedded = self.embedding(difference)
        agg = self.aggregate(embedded)
        output = torch.cat((samples,agg),1) 
        return output

    def calculate_difference(self,x):
        #difference = torch.zeros(list(x.size()) + [x.size()[0]])
        #difference = torch.zeros([x.size()[0]] + [x.size()[1]] + [x.size()[0]+1])
        difference = torch.zeros([x.size()[0]] + [x.size()[1]] + [x.size()[0]])
        #mean = torch.mean(x)
        for i,point in enumerate(x):
            difference[i] = (x-point).transpose(0,1)
            #difference[i] = ((x-point)**2).transpose(0,1)
            #difference[i] = ((x - point)**2).transpose(0,1) + torch.mean(x)
            #difference[i] = torch.cat((((x - point)**2)[:,0],point-mean))
        return difference

    def embedding(self,x):
        embedder = nn.Sequential(
            nn.Conv1d(2,256,1),
            #nn.Conv1d(2,156,1),
            #nn.Conv1d(2,400,1),
            nn.LeakyReLU(),
            nn.Conv1d(256,256,1),
            #nn.Conv1d(156,156,1),
            #nn.Conv1d(400,400,1),
            nn.LeakyReLU(),
            nn.Conv1d(256,256,1),
            nn.LeakyReLU(),
            #nn.Conv1d(256,256,1),
            #nn.LeakyReLU(),
            #nn.Conv1d(256,256,1),
            #nn.LeakyReLU(),

           # nn.Conv1d(400,400,1),
           # nn.LeakyReLU(),
           # nn.Conv1d(400,400,1),
           # nn.LeakyReLU(),
           # nn.Conv1d(400,400,1),
           # nn.LeakyReLU(),


            nn.Conv1d(256,32,1),
            #nn.Conv1d(156,15,1),
            #nn.Conv1d(400,60,1),
            nn.LeakyReLU()
        )
        return embedder(x)
    
    def aggregate(self,embedded):
        output = torch.zeros([embedded.size()[0],embedded.size()[1]])
        for i,point in enumerate(embedded):
            for j,b in enumerate(point):
                output[i,j] = torch.std(b)
                #output[i,j] = torch.mean(b)
                #output[i,j] = torch.sum(b)
                #output[i,j] = torch.median(b)

        return output

    def train_generator(self,batch_size,real_samples,roll_steps):
        latent_space_samples = torch.randn((batch_size, 1))
        
        # Training the generator
        self.generator.zero_grad()
        generated_samples = self.generator(latent_space_samples)
        generated_samples = self.per_batch_statistics(generated_samples)
        
        loss_discriminator_unrolled = nn.BCELoss()

        if roll_steps > 0:
            discriminator_backup = copy.deepcopy(self.discriminator)
            optimizer_discriminator_unrolled = torch.optim.Adam(discriminator_backup.parameters(),lr=self.lr_init)
            scheduler_discriminator_unrolled = LambdaLR(optimizer_discriminator_unrolled, self.smooth_learning_rate)
            for i in range(roll_steps):
                self.train_discriminator_unrolled(discriminator_backup,optimizer_discriminator_unrolled,scheduler_discriminator_unrolled,batch_size,real_samples,generated_samples,loss_discriminator_unrolled,i,roll_steps)
           
        output_discriminator_generated = self.discriminator(generated_samples)
        loss_generator = self.loss_function_generator(
            output_discriminator_generated, self.real_samples_labels
        )

        loss_generator.backward(retain_graph=False)
        self.optimizer_generator.step()

        if roll_steps > 0:
            #self.discriminator = discriminator_backup
            del discriminator_backup
        return loss_generator,generated_samples 

    def generate_samples(self,batch_size):
        latent_space_samples = torch.randn((self.graphing_batch_size, 1))
        generated_samples = self.generator(latent_space_samples)
        return generated_samples.detach()

