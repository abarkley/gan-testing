import torch
from torch import nn

import matplotlib.pyplot as plt

import distribution

from gan_2D import Gan_Trainer

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(34,256), 
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.5),
            nn.Linear(256,2)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    trainer = Gan_Trainer(seeded=True)
    trainer.train(Generator(),
                  Discriminator(),
                  distribution=distribution.camel_back_2d,
                  data_shift=[lambda x : x*8, lambda y : y*8],
                  batch_size=10,
                  data_size=150,
                  lr = [5*10**-3,5*10**-6 * .9**10],
                  xlim=[-10,10],
                  ylim=[-10,10])