import torch
from torch import nn

import matplotlib.pyplot as plt

import distribution

import argparse

from gan_2D_unrolled import Gan_Trainer

class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(34,256), 
            #nn.Linear(62,256),
            #nn.Linear(17,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,1),
            nn.Sigmoid(),
        )

    def forward(self,x):
        return self.model(x)

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(1,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,256),
            nn.LeakyReLU(),
            nn.Dropout(0.50),
            #nn.Dropout(0.75),
            nn.Linear(256,2)
        )

    def forward(self,x):
        output = self.model(x)
        return output

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-N','-n','--name',default='gan',help='name of the results file and graph')
    parser.add_argument('-i',default = 1,type=int,help='number of gans to train')
    parser.add_argument('-e,--epochs',default = 1000,type=int,help='number of epochs to train for')
    parser.add_argument('-r',default = 5,type=int,help='number of steps to unroll')
    parser.add_argument('--graph_path',default="/home/barkleya/gan/graphs/gauss_2d/",help='location where graph will be saved')
    parser.add_argument('--result_path',default="/home/barkleya/gan/gauss_2d/results",help='location where the results file will be saved')
    parser.add_argument('-g',action='store_false',help='set to not draw graphs')
    parser.add_argument('-s',action='store_true',help='set to use a seed')
    parser.add_argument('-v',action='store_true',help='set to display more information during the run')
    args = vars(parser.parse_args())

    iterations = args['i']
    for i in range(0,iterations):
        trainer = Gan_Trainer(seeded=args['s'])
        trainer.train(Generator(),
                      Discriminator(),
                      distribution=distribution.camel_back_larger_right_mean,
                      data_shift=[lambda x : x*10, lambda y : y*8],
                      num_epochs=args['e,__epochs'],
                      batch_size=15,
                      data_size=150,
                      lr = [5*10**-3,5*10**-5],
                      iteration=i,
                      roll_steps=args['r'],
                      xlim=[-10,10],
                      ylim=[-10,10],
                      quantiles = [[-4.84162,-4.25335,-3.74665,-3.15838,1.901,7.15838,7.74665,8.25335,8.84162],[-4.84162,-4.25335,-3.74665,-3.15838,0,3.15838,3.74665,4.25335,4.84162]],
                      graph_path = args['graph_path'],
                      results_path = args['result_path'],
                      name = args['name'],
                      draw_graphs = args['g'],
                      verbose = args['v'])
