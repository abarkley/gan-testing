import torch
from torch import nn

def camel_back_2d(size):
    x = camel_back(size,4.0,-4.0)
    y = camel_back(size,4.0,-4.0)
    dist = torch.cat((x.view(size,1),y.view(size,1)),1)
    return dist

def camel_back_2d_reflected_y_axis(size):
    x = camel_back(size,-4.0,4.0)
    y = camel_back(size,4.0,-4.0)
    dist = torch.cat((x.view(size,1),y.view(size,1)),1)
    return dist

def camel_back_2d_reflected_x_axis(size):
    x = camel_back(size,4.0,-4.0)
    y = camel_back(size,-4.0,4.0)
    dist = torch.cat((x.view(size,1),y.view(size,1)),1)
    return dist

def camel_back_2d_upper(size):
    x = camel_back(size,4.0,-4.0)
    y = camel_back(size,4.0,4.0)
    dist = torch.cat((x.view(size,1),y.view(size,1)),1)
    return dist

def camel_back_larger_right_mean(size):
    x = camel_back(size,8.0,-4.0,10)
    y = camel_back(size,4.0,-4.0,8)
    dist = torch.cat((x.view(size,1),y.view(size,1)),1)
    return dist

def camel_back(size,right_mean,left_mean,shift=8):
    sample1 = torch.normal(right_mean,.5,(size//2,)) / shift
    sample2 = torch.normal(left_mean,.5,(size//2,)) / shift
    dist = torch.cat((sample1,sample2),0)
    return dist

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    dist = camel_back_2d(1000)
    plt.hist2d(list(dist[:,0]),list(dist[:,1]),bins=[100,100])
    #plt.contour([list(dist[:,0]),list(dist[:,1])])
    plt.show()
