import torch
from torch import nn
from torch.optim.lr_scheduler import LambdaLR,ExponentialLR

import math
import matplotlib.pyplot as plt

from collections import deque

class Gan_Trainer:
    class Grapher:
        def __init__(self):
            plt.ion()
            self.fig,self.axs = plt.subplots(2,3)
            self.fig.set_size_inches(16,20)

            self.ax_points = self.axs[0,0]
            self.ax_chi = self.axs[0,1]
            self.ax_mean = self.axs[0,2]
            self.ax_violations = self.axs[1,0]
            self.ax_hist = self.axs[1,1]

            self.ax_points.set_title("Generated Points")
            self.ax_chi.set_title("Chi Error")
            self.ax_mean.set_title("Chi Error Mean")
            self.ax_violations.set_title("Elements outside of range")
            self.ax_hist.set_title("Distrbution of generated points")

        def plot_generated_points(self,generate_samples,shift_function,xlim,ylim):
            self.ax_points.clear()
            self.ax_points.set_ylim(ylim)
            self.ax_points.set_xlim(xlim)
            self.ax_points.set_autoscale_on(False)
            self.ax_points.title.set_text("Generated Points")

            self.ax_points.plot(shift_function(generate_samples[:,0]), generate_samples[:,1],".")
            
        def plot_chi_points(self,chi,epoch):
            self.ax_chi.plot(epoch,chi,'.')

        def plot_mean_chi_points(self,chi_avg,epoch):
            self.ax_mean.plot(epoch,chi_avg,'.')
        
        def plot_elements_out_of_range(self,num_violations,epoch):
            self.ax_violations.plot(epoch,num_violations,'.')

        def plot_histogram(self,generate_samples,shift_function,xlim):
            self.ax_hist.clear()
            self.ax_hist.set_title("Distrbution of generated points")

            self.ax_hist.hist(shift_function(generate_samples[:,0]),20,xlim)

        def redraw(self):
            self.fig.canvas.draw()

    class Data_Analyser:
        def __init__(self,num_epochs):
            self.num_data_points = num_epochs / 100
            self.chi_data = deque()
            self.chi_avg = 0

        def chi_squared(self,generated_samples, func, shift_function):
            # Assuming right now that the generated_samples are 2 dimensional.
            n = generated_samples[:,0].size()[0]
            chi_squared = 0
            for i in range(0,n):
                x = generated_samples[:,0][i].item()
                gen_y = generated_samples[:,1][i].item()
                y = func(shift_function(torch.tensor(x)))
                chi_squared += ((gen_y-y)**2)/abs(y)
            return chi_squared

        def sma(self,chi):
            if len(self.chi_data) == self.num_data_points:
                last = self.chi_data.pop()
                self.chi_avg += (1/self.num_data_points) * (chi-last)
            else:
                self.chi_data.insert(0,chi)
                self.chi_avg = sum(self.chi_data) / len(self.chi_data)
            return self.chi_avg

        def outside_valid_range(self,generated_samples):
            valid_range = [-1,1]
            x = generated_samples[:,0].tolist()
            return len(list(filter(lambda elem: elem < valid_range[0] or elem > valid_range[1],x)))

    def __init__(self,seeded=False):
        if seeded:
            torch.manual_seed(111)

    def smooth_learning_rate(self,epoch):
        return math.exp(self.m * (epoch - 1))

    def data_setup(self,data_function,data_shift,batch_size,data_size):
        #train_data_length = 1024
        train_data_length = data_size
        train_data = torch.zeros((train_data_length,2))

        train_data[:, 0] = (torch.rand(train_data_length)-.5)*2 
        train_data[:, 1] = data_function(data_shift(train_data[:,0]))

        train_labels = torch.zeros(train_data_length)
        train_set = [
            (train_data[i],train_labels[i]) for i in range(train_data_length)
        ]
        self.train_loader = torch.utils.data.DataLoader(train_set,batch_size=batch_size,shuffle=True)

        self.real_samples_labels = torch.ones((batch_size, 1))

    def train(self,
              generator,
              discriminator,
              data_function = torch.sin,
              data_shift = lambda x : x*math.pi,
              num_epochs=1000,
              batch_size=32,
              data_size=1024,
              lr=[0.001,0.001],
              loss_function = nn.BCELoss(),
              xlim = [-1.5*math.pi,1.5*math.pi],
              ylim = [-2,2],
              draw_graphs = True):

        self.graphing_batch_size = 1000

        self.grapher = self.Grapher()
        self.data_analyser = self.Data_Analyser(num_epochs)

        self.loss_function = loss_function
        self.generator = generator
        self.discriminator = discriminator

        self.data_setup(data_function,data_shift,batch_size,data_size)

        if type(lr) is list:
            self.lr_init = lr[0]
            self.lr_end = lr[1]
        else:
            self.lr_init = self.lr_end = lr
        self.m = math.log(self.lr_end/self.lr_init) / (num_epochs-1)

        #TODO: be able to change this?
        self.optimizer_discriminator = torch.optim.Adam(discriminator.parameters(),lr=self.lr_init)
        self.optimizer_generator = torch.optim.Adam(generator.parameters(),lr=self.lr_init)

        self.scheduler_discriminator = LambdaLR(self.optimizer_discriminator, self.smooth_learning_rate)
        self.scheduler_generator = LambdaLR(self.optimizer_generator, self.smooth_learning_rate)

        for epoch in range(num_epochs):
            print("new epoch")
            for n, (real_samples, _) in enumerate(self.train_loader):
                loss_discriminator = self.train_discriminator(batch_size,real_samples)
                loss_generator,_= self.train_generator(batch_size)

                # Show loss
                if epoch % 10 == 0 and n == batch_size - 1:
                                       
                    generate_samples = self.generate_samples(batch_size)

                    chi = self.data_analyser.chi_squared(generate_samples,data_function,data_shift)
                    chi_avg = self.data_analyser.sma(chi)
                    num_violations = self.data_analyser.outside_valid_range(generate_samples)

                    print(f"Epoch: {epoch} Loss D.: {loss_discriminator}")
                    print(f"Epoch: {epoch} Loss G.: {loss_generator}")
                    print(f"Epoch: {epoch} lr: {self.optimizer_generator.param_groups[0]['lr']}")
                    print(f"Epoch: {epoch} violations: {num_violations}")
                    print(f"Epoch: {epoch} chi squared error.: {chi}")
                    print(f"Epoch: {epoch} chi squared error mean.: {chi_avg}")

                    if draw_graphs:
                        self.grapher.plot_generated_points(generate_samples,data_shift,xlim,ylim)
                        self.grapher.plot_chi_points(chi,epoch)
                        self.grapher.plot_mean_chi_points(chi_avg,epoch)
                        self.grapher.plot_elements_out_of_range(num_violations,epoch)
                        self.grapher.plot_histogram(generate_samples,data_shift,xlim)
                        self.grapher.redraw()

            self.scheduler_discriminator.step()
            self.scheduler_generator.step()

        if draw_graphs:
            plt.show(block=True)

    def train_discriminator(self,batch_size,real_samples):
        # Data for training the discriminator
        latent_space_samples = torch.randn((batch_size, 2))
        generated_samples = self.generator(latent_space_samples)
        generated_samples_labels = torch.zeros((batch_size, 1))
        all_samples = torch.cat((real_samples, generated_samples))
        all_samples_labels = torch.cat(
            (self.real_samples_labels, generated_samples_labels)
        )
        
        # Training the discriminator
        self.discriminator.zero_grad()
        output_discriminator = self.discriminator(all_samples)
        loss_discriminator = self.loss_function(
            output_discriminator, all_samples_labels)
        loss_discriminator.backward()
        self.optimizer_discriminator.step()
        return loss_discriminator

    def train_generator(self,batch_size):
        latent_space_samples = torch.randn((batch_size, 2))

        # Training the generator
        self.generator.zero_grad()
        generated_samples = self.generator(latent_space_samples)
        output_discriminator_generated = self.discriminator(generated_samples)
        loss_generator = self.loss_function(
            output_discriminator_generated, self.real_samples_labels
        )
        loss_generator.backward()
        self.optimizer_generator.step()
        return loss_generator,generated_samples 

    def generate_samples(self,batch_size):
        latent_space_samples = torch.randn((self.graphing_batch_size, 2))
        generated_samples = self.generator(latent_space_samples)
        return generated_samples.detach()